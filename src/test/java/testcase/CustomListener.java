package testcase;

import com.aventstack.extentreports.MediaEntityBuilder;
import driver.DriverManager;
import io.qameta.allure.Attachment;
import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.IOException;

import static klovreporterconfigs.KlovConfigs.extentTest;
import static logger.CustomLogger.*;

public class CustomListener implements ITestListener {

    private static String getTestMethodName(ITestResult iTestResult) {
        return iTestResult.getMethod().getConstructorOrMethod().getName();
    }

    @Attachment
    private byte[] saveScreenShot(WebDriver driver) {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    @Attachment(value = "{0}", type = "text/plain")
    private static String saveTextLog(String message) {
        return message;
    }


    @Override
    public void onStart(ITestContext iTestContext) {
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        logInfo("I am in onFinish method " + iTestContext.getName());
    }

    @Override
    public void onTestStart(ITestResult iTestResult) {
        logInfo("I am in onTestStart method " + getTestMethodName(iTestResult) + " start");
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        logInfo("I am in onTestSuccess method " + getTestMethodName(iTestResult) + " succeed");
        WebDriver driver = DriverManager.getDriver();
        String base64string = "";
        if (driver != null) {
            logInfo("Screenshot captured for test case:" + getTestMethodName(iTestResult));
            byte[] screenshot = saveScreenShot(driver);
            base64string = new String(Base64.encodeBase64(screenshot));
        }
        try {
            extentTest.info("Capturing screenshot on TestSuccess",
                    MediaEntityBuilder.createScreenCaptureFromBase64String(base64string).build());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        logError("I am in onTestFailure method " + getTestMethodName(iTestResult) + " failed");
        WebDriver driver = DriverManager.getDriver();
        String base64string = "";
        if (driver != null) {
            logError("Error message: " + iTestResult.getThrowable().getMessage());
            logWarn("Screenshot captured for test case:" + getTestMethodName(iTestResult));
            byte[] screenshot = saveScreenShot(driver);
            base64string = new String(Base64.encodeBase64(screenshot));
        }
        saveTextLog(getTestMethodName(iTestResult) + " failed and screenshot taken!");
        try {
            extentTest.fail("Capturing screenshot on TestFailure",
                    MediaEntityBuilder.createScreenCaptureFromBase64String(base64string).build());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        logWarn("I am in onTestSkipped method " + getTestMethodName(iTestResult) + " skipped");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
        logWarn("Test failed but it is in defined success ratio " + getTestMethodName(iTestResult));
    }
}
