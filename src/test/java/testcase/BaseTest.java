package testcase;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import driver.DriverManager;
import klovreporterconfigs.KlovConfigs;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import java.lang.reflect.Method;

import static utils.PropertyFileHandler.MAIN_URL;
import static utils.Utils.goToPageURL;

public class BaseTest extends KlovConfigs {

    @BeforeSuite
    public void beforeSuite() {
        setExtentHtmlReporter();
        setTestName("Verify Sending and Deleting of a letter");
    }

    @BeforeMethod
    public void beforeMethod(Method method) {
        setTestName(method.getName());
        goToPageURL(MAIN_URL);
    }

    @AfterMethod
    public void afterMethod(ITestResult result) {
        DriverManager.quitDriver();
        if (result.getStatus() == ITestResult.FAILURE) {
            extentTest.log(Status.FAIL, MarkupHelper.createLabel(result.getName() + " FAILED ", ExtentColor.RED));
            extentTest.fail(result.getThrowable());
        } else if (result.getStatus() == ITestResult.SUCCESS) {
            extentTest.log(Status.PASS, MarkupHelper.createLabel(result.getName() + " PASSED ", ExtentColor.GREEN));
        } else {
            extentTest.log(Status.SKIP, MarkupHelper.createLabel(result.getName() + " SKIPPED ", ExtentColor.ORANGE));
            extentTest.skip(result.getThrowable());
        }
    }

    @AfterSuite
    public void afterSuiteFlush() {
        KlovConfigs.extent.flush();
        extentHtmlReporterQuit();
    }
}
