package businessobject;

import pageobject.*;

import static logger.CustomLogger.logInfo;
import static wait.CustomWait.waitUntilDocumentReadyState;

public class DeletingLettersBO {

    private SentLettersPage sentLettersPage;

    public DeletingLettersBO() {
        sentLettersPage = new SentLettersPage();
    }

    public void deleteLastLetter() {
        sentLettersPage.selectLastLetter();
        sentLettersPage.deleteSelectedLetter();
    }

    public void cleanUpMailBox() {
        logInfo("Deleting default letter to clean up the mail box ");
        sentLettersPage.selectLastLetter();
        sentLettersPage.deleteSelectedLetter();
        waitUntilDocumentReadyState();
        logInfo("Default letter deleted ");
    }
}
