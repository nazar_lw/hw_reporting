package logger;

import com.aventstack.extentreports.Status;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static klovreporterconfigs.KlovConfigs.extentTest;

public class CustomLogger {

    private static final Logger logger = LogManager.getLogger(CustomLogger.class);

    @Step("{0}")
    public static void logInfo(String log) {
        logger.info(log);
        extentTest.log(Status.INFO, log);
    }

    @Step("{0}")
    public static void logDebug(String log) {
        logger.debug(log);
        extentTest.log(Status.DEBUG, log);
    }

    @Step("{0}")
    public static void logError(String log) {
        logger.error(log);
        extentTest.log(Status.ERROR, log);
    }

    @Step("{0}")
    public static void logWarn(String log) {
        logger.warn(log);
        extentTest.log(Status.WARNING, log);
    }
}