package klovreporterconfigs;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.KlovReporter;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class KlovConfigs {

    private static ExtentHtmlReporter extentHtmlReporter;
    public static ExtentReports extent;
    public static ExtentTest extentTest;

    protected void setExtentHtmlReporter() {
        extentHtmlReporter = new ExtentHtmlReporter("Report.html");
        extent = new ExtentReports();
        Calendar calendar = new GregorianCalendar();
        KlovReporter klov = new KlovReporter();
        ServerAddress serverAddress =
                new ServerAddress("ec2-18-219-202-217.us-east-2.compute.amazonaws.com", 27017);

        MongoCredential mongoCredential =
                MongoCredential.createCredential("root", "admin", "2419839".toCharArray());
        List<MongoCredential> credentials = new ArrayList<>();
        credentials.add(mongoCredential);

        klov.initMongoDbConnection(serverAddress, credentials);

        klov.setProjectName("Test-Gmail");

        klov.setKlovUrl("ec2-18-219-202-217.us-east-2.compute.amazonaws.com:7777");
        klov.setReportName("Report Test Gmail" + calendar.getTime());
        extent.attachReporter(extentHtmlReporter, klov);
    }

    public void setTestName(String testName) {
        extentTest = extent.createTest(testName);
    }

    public void extentHtmlReporterQuit() {
        if (extentHtmlReporter != null) {
            extentHtmlReporter = null;
        }
    }
}
