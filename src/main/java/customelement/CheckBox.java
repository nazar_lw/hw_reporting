package customelement;

import customelement.abstraction.Element;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static logger.CustomLogger.logDebug;
import static logger.CustomLogger.logError;
import static wait.CustomWait.getNewFluentWait;

public class CheckBox extends Element {

    public CheckBox(WebElement webElement) {
        super(webElement);
    }

    public void setChecked() {
        logDebug("Checking the customelement: " + getLocator());
        if (!isChecked()) {
            webElement.click();
        }
    }

    private boolean isChecked() {
        boolean flag = false;
        try {
            logDebug("Waiting and Verifying if Custom check-box: " + getLocator() + " is checked");
            flag = getNewFluentWait().until(ExpectedConditions.visibilityOf(webElement)).isSelected();
        } catch (NoSuchElementException | TimeoutException e) {
            logError("While looking for the Custom element: " + getLocator() + " throw an Exception: " + e.getMessage());
        }
        return flag;
    }
}