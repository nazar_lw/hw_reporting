package customelement;

import customelement.abstraction.Element;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static logger.CustomLogger.*;
import static wait.CustomWait.getNewFluentWait;
import static wait.CustomWait.waitUntilDocumentReadyState;

public class PopUp extends Element {

    public PopUp(WebElement webElement) {
        super(webElement);
    }

    public void openPopUp() {
        logDebug("Waiting and opening Custom popup: " + getLocator());
        waitUntilDocumentReadyState();
        getNewFluentWait().until(ExpectedConditions.visibilityOf(webElement)).click();
    }

    public boolean isDisplayed() {
        try {
            logInfo("Checking if custom popup: ( " + getLocator() + " ) is displayed");
            return getNewFluentWait().until(ExpectedConditions.visibilityOf(webElement)).isDisplayed();
        } catch (NoSuchElementException | TimeoutException e) {
            logWarn("Custom element: ( " + getLocator() + " ) is NOT displayed");
            return false;
        }
    }
}
