package pageobject;

import customdecorator.CustomFieldDecorator;
import driver.DriverManager;
import org.openqa.selenium.support.PageFactory;

import static logger.CustomLogger.logInfo;

abstract class AbstractPage {

    AbstractPage() {
        logInfo("Initialize PageFactory in: " + this.getClass().getName());
        PageFactory.initElements(new CustomFieldDecorator(DriverManager.getDriver()), this);
    }
}
