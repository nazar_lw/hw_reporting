package pageobject;

import customelement.Button;
import org.openqa.selenium.support.FindBy;

import static logger.CustomLogger.logInfo;

public class HandleLettersPage extends AbstractPage {

    @FindBy(css = "div.T-I.T-I-KE.L3")
    private Button composeButton;

    @FindBy(css = "div.TN.bzz.aHS-bnu")
    private Button getAllSentLettersButton;

    public void openCreateLetterForm() {
        logInfo("Opening a new letter form");
        composeButton.waitAndClick();
    }

    public void getAllSentLettersPage() {
        logInfo("Opening the page with sent letters");
        getAllSentLettersButton.waitWithPollingUntilDocumentIsReadyAndClick();
    }
}
