package pageobject;

import customelement.Button;
import customelement.PopUp;
import org.openqa.selenium.support.FindBy;

import static logger.CustomLogger.logInfo;
import static logger.CustomLogger.logWarn;

public class HomePage extends AbstractPage {

    @FindBy(xpath = "//img[@class='gb_La gbii']")
    private PopUp accountWindow;

    @FindBy(id = "gb_71")
    private Button exitButton;

    public boolean areAccountOptionsPresent() {
        logWarn("Verifying if account window is displayed ");
        return accountWindow.isDisplayed();
    }

    public void logOut() {
        logInfo("Log out from the Account");
        accountWindow.openPopUp();
        exitButton.waitAndClick();
    }
}
