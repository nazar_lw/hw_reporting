The project to test Logination, Creation of a new Letter, and Deleting it for Gmail post service.

**Design patterns:**

 - Page Object with @FindBy annotations and initialization via org.openqa.selenium.support.PageFactory.
 
 - Business Object with encapsulation of business logic 

 - Custom decorator which enables custom elements with specific methods. Decorator itself works with reflection to provide necessary handling of @FindBy annotations.

 
 
**Precondition of test** - Sending of a default letter to compare with the main letter by their recipient and compare the size of sent letters list before and after deleting of the main letter. Sending is done with the help of smtp mail sender. All accounts from src/main/resources/userdata.json already support less secure apps. In order to work with a new account- logIn to google account and run the following url - https://myaccount.google.com/lesssecureapps?utm_source=google-account&utm_medium=web&hl=uk

**Parallel run** - Test uses DataProvider with 5 user-data options. Initially it runs 3 threads and 2 more after it. To run test - tape _mvn clean test_ from the command line.               

**Reporting:** 

- Allure. Reports generated to target/allure-results. 
Recent reports with all logs and screenshots 

https://fervent-wilson-14ebae.netlify.app/

- ExtentReports. The project uses configuration of ExtentReports instantiated in klovreporterconfigs package. It writes logs to report alongside with allure and log4j in logger package and listens to the tests from testNg CustomListener. Reports are generated to both: Report.html file in the root of project and to the remote mongo-database via secure url. 
The link to remote server:
http://ec2-18-219-202-217.us-east-2.compute.amazonaws.com:7777/

  Credentials: klovadmin/password. This test-project is under the name: Test-Gmail.

    It is also possible to run klov-server locally and connect to the DB via endpoint:
mongodb://root:2419839@ec2-18-219-202-217.us-east-2.compute.amazonaws.com:27017/klov?authSource=admin

    To download klov-server: https://extentreports.com/docs/klov/#